window.addEventListener('DOMContentLoaded', () => {

  // setup form for login
  const form = document.getElementById('login-form');
  form.addEventListener('submit', async event => {
    event.preventDefault();

  //   const data = Object.fromEntries(new FormData(form))
    const fetchOptions = {
      method: 'post',

      // djwto takes only normal forms, not JSON 

      body: new FormData(form),

      // sends credentials in fetch
      credentials: 'include',
      
      // headers: { 
      //   'Content-Type': 'application/json',
      // }
    };
    const url = 'http://localhost:8000/login/';
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      window.location.href = '/';
    } else {
      console.error(response);
    }
  });
});