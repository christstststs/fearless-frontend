window.addEventListener("DOMContentLoaded", async () => {
    const URL = "http://localhost:8000/api/locations/";
    try {
        const response = await fetch(URL);
        if (!response.ok) {
            throw new Error("Response not ok");
        } else {
            const data = await response.json();
            //console.log(data)
            let selectTag = document.getElementById("location");
            data.locations.forEach((location) => {
                let option = document.createElement("option");
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            });
        }
    } catch (e) {
        alert(e);
    }

    const formTag = document.getElementById("create-conference-form");

    formTag.addEventListener("submit", async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const dataObject = Object.fromEntries(formData);
        const json = JSON.stringify(dataObject);
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        console.log(json);
        let fetchOptions = {
            method: "post",
            body: json,
            headers: {
                "Content-type": "application/json",
            },
        };
        const response = await fetch(conferenceUrl, fetchOptions);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }else{console.log(response)}
    });
});
